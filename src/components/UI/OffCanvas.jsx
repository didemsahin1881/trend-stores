import { Fragment } from "react";
import ReactDOM from "react-dom";

import "./OffCanvas.css"; 

const BackDrop = (props) => {
return  <div className="backdrop" onClick={props.onClose}></div>;
}

const OffCanvasOverlay = (props) => {
return (
  <div className='offcanvas'>
  <div className="content">
    {
    props.children
    }
  </div>
</div>
)
}

const OffCanvas = (props) => {
  const portalElement = document.getElementById("overlays"); 
  return (
    <Fragment>
    {ReactDOM.createPortal(<OffCanvasOverlay>{props.children}</OffCanvasOverlay>, portalElement)}
      {ReactDOM.createPortal(  <BackDrop onClose={props.onClose}></BackDrop>, portalElement)}
     
    </Fragment>
  )
};

export default OffCanvas