import PropTypes from 'prop-types';
import './Card.css';

function Card(props) {
  return (
    <li className="card">{props.children}</li>
  );
}

Card.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Card;