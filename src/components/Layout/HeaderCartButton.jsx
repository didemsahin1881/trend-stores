 import {useContext} from "react";
 import { CartContext } from "../../context/CartProvider";

 import CartIcon from "../Cart/CartIcon";
import "./HeaderCartButton.css";

function HeaderCartButton({ onShowCart }) { 
  const cartCtx = useContext(CartContext);
  const totalItemsInCart = cartCtx.items.reduce((accumulator, currentItem)=> {
    return accumulator + currentItem.amount
  },0) ;
  console.log(cartCtx);
  return (
    <button className="button" onClick={onShowCart}>
        <span className="icon">
        <CartIcon></CartIcon>
        </span>
        <span>Sepetim</span>
        <span className="badge"> {totalItemsInCart}</span>
    </button>
  )
}

export default HeaderCartButton