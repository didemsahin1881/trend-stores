import "./Header.css" 
import HeaderCartButton from "./HeaderCartButton"

function Header({onShowCart}) {
  return (
    <header className="header">
        <h1>
            Didem Store
        </h1>
        <HeaderCartButton onShowCart={onShowCart}></HeaderCartButton>
    </header>
  )
}

export default Header